# Access_Operators_NoSQL_Systems

## Repository Structure 
The Access_Operators_NoSQL_Systems Repository includes two (2) directories.

##Directory 1 - Containers
Directory "Containers" includes the files need to run two (2) containers in docker environment. The first container would host
MongoDB and the second one Hbase.
 
##Directory 2 - Source Code 
Directory "Source Code " includes a maven java project in order to test the implemented access operators for the selected NoSQL Systems.

## Selected NoSQL Systems

### Mongo Database
MongoDB is an open-source database used by companies of all sizes, across all industries and for a wide variety of applications. 
It is an agile database that allows schemas to change quickly as applications evolve, while still providing the functionality developers 
expect from traditional databases, such as secondary indexes, a full query language and strict consistency. MongoDB is built for scalability, 
performance and high availability, scaling from single server deployments to large, complex multi-site architectures.

### Apache Hbase
Apache HBase� is the Hadoop database, a distributed, scalable, big data store. HBase provides Bigtable-like capabilities on top of Hadoop and HDFS.

## Getting Started

### Prerequisites

Install Apache Maven Project
```sh
  apt-cache search maven
  sudo apt-get install maven
```

Install Docker
```sh
  sudo apt-get update
  sudo apt-get install docker-ce
  
```

Verify that Docker CE is installed correctly by running the hello-world image.
```sh
  sudo docker run hello-world 
```

#### Usage

* First, build the images of the containers and run them. Use the following commands:     
	*  cd hbase-cont      
	*  docker build -t debian-hbase.       
	*  docker run -d -h myhbase -p 2181:2181 -p 60000:60000 -p 60010:60010 -p 60020:60020 -p 60030:60030 --name pyke debian-hbase    
	*  cd mongo-cont      
	*  docker-compose up    
	
* For the hbase to run properly you have to add the following in your /etc/hosts file    
	 *  <host_machine_ip> myhbase    

*  After the above steps you have to run the maven project    
	*  Use any Java IDE (Eclipse ise the preferred one)    
	*  Import existing maven project    
	*  Run class MyApplication.java

## API Documentation    

The API methods implemented are the following:    

### MongoApi

*  **Make connection to MongoDB** - MongoConnect(String host_name, int host_port, String dbname)
*  **Close connection to MongoDB** - close_connection()
*  **Create a Mongo collection**- create_collection(String db_name, String collection_name)
*  **List all the available collections inside a Mongo database** - collectionNames(String db_name)
*  **Insert a document into a collection** - insert_doc(String db_name, String collection_name, Document Doc)
*  **Delete a document from a collection** - del_doc(String db_name, String collection_name, Bson filter)
*  **List all the available documents in a collection** - get_docs(String db_name, String collection_name) 
*  **Find a document in a collection** - find_doc(String db_name, String collection_name, Bson filter)
*  **Update a document** - update_doc(String db_name, String collection_name, Bson filter, Bson filter_update)
*  **Delete (drop) a collection from a Mongo database** - drop_collection(String db_name, String collection_name)
*  **Scan all table data** - scan_table(TableName table_name, String family, String qualifier)
*  **Update table data (update a specific row)** - update_table(TableName tableName, String rowId, String family, String qualifier, String value)
*  **Scan table rows based on a filter** - filter_table(TableName table_name, String family, String qualifier, List<Filter> filtering)

### HBaseApi 
*  **Connect to HBASE** - connectHBase(String host, int port, String path) 
*  **Close connection to HBase** - closeHBase()
*  **Create table with family** - create_table(TableName tableName, String family)
*  **Delete a table** - del_table(TableName tableName)
*  **Check if specific table exist** - table_exist(TableName table_name)
*  **Disable a table** - disable_table(TableName table_name)
*  **Enable a table** - enable_table(TableName table_name) 
*  **Add column to table** - add_column_to_table(TableName table_name, String column)
*  **Delete column from table**  - delete_column(TableName table_name, String column)
*  **Add data to column family** -  add_data(TableName tableName, String rowId, String family, String qualifier, String value)

## Lead Developer
Evgenia Kapassa (evgeniakapassa@gmail.com)








