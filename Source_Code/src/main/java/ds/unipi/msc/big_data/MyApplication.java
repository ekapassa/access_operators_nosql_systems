package ds.unipi.msc.big_data;

import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.filter.BinaryComparator;
import org.apache.hadoop.hbase.filter.Filter;
import org.apache.hadoop.hbase.filter.PrefixFilter;
import org.apache.hadoop.hbase.filter.QualifierFilter;
import org.apache.hadoop.hbase.filter.CompareFilter.CompareOp;
import org.bson.Document;
import org.bson.conversions.Bson;

/** Mongo Client Imports **/
import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Filters.gt;
import static com.mongodb.client.model.Filters.gte;
import static com.mongodb.client.model.Filters.lt;
import static com.mongodb.client.model.Filters.lte;
import static com.mongodb.client.model.Updates.set;

import java.util.Arrays;
import java.util.List;

public class MyApplication {

	public static void main(String[] args) {

		System.out.println("WELCOME TO THE BIG-DATA PROJECT :) ");
		System.out.println("Uncomment one of the following to start 'playing' with this Database Interface.");

		// mongo_app();
		// hbase_app();

	}

	/**
	 * HBASE APP
	 **/
	private static void hbase_app() {

		/** HBase config **/
		final String host = "myhbase";
		final int port = 2181;
		final String path = "/hbase";

		HBaseApi hbase_app = new HBaseApi();

		/** Initialize vars **/
		TableName tableName = TableName.valueOf("Blog_Post");
		String family = "blog_authors";
		String column = "Author";
		String rowId = "author_id2";
		String qualifier = "test";
		String value = "Kapassa";

		/** Filters **/
		Filter fix_filter = new PrefixFilter(rowId.getBytes());
		Filter equal = new QualifierFilter(CompareOp.EQUAL, new BinaryComparator(qualifier.getBytes()));
		Filter gteFilter = new QualifierFilter(CompareOp.GREATER_OR_EQUAL, new BinaryComparator(qualifier.getBytes()));
		Filter gtFilter = new QualifierFilter(CompareOp.GREATER, new BinaryComparator(qualifier.getBytes()));
		Filter lteFilter = new QualifierFilter(CompareOp.LESS_OR_EQUAL, new BinaryComparator(qualifier.getBytes()));
		Filter ltFilter = new QualifierFilter(CompareOp.LESS, new BinaryComparator(qualifier.getBytes()));
		List<Filter> filters = Arrays.asList(fix_filter, equal);

		/** Connect to HBASE **/
		// hbase_app.connectHBase(host, port, path);

		/** Create table with family **/
		// hbase_app.create_table(tableName, family);

		/** Delete table **/
		// hbase_app.del_table(tableName);

		/** Check if table exists **/
		// hbase_app.table_exist(tableName);

		/** Disable a table **/
		// hbase_app.disable_table(tableName);

		/** Enable a table **/
		// hbase_app.enable_table(tableName);

		/** Add column to table **/
		// hbase_app.add_column_to_table(tableName, column);

		/** Delete column from table **/
		// hbase_app.delete_column(tableName, column);

		/** Add data to table **/
		// hbase_app.add_data(tableName, rowId, family, qualifier, value);

		/** Scan table **/
		// hbase_app.scan_table(tableName, family, qualifier);

		/** Updata table data **/
		// hbase_app.update_table(tableName, "author_id", family, qualifier, "Marios");

		/** Scan table by filter **/
		// hbase_app.filter_table(tableName, family, qualifier, filters);

	}

	/**
	 * MONGO APP
	 **/
	private static void mongo_app() {

		MongoApi mongo_app = new MongoApi();

		final String host = "localhost";
		final int port = 27017;

		final String dbname = "Evgenia_DB";
		final String collectionName = "my_music";

		// Define Filters
		Bson equal_filter = eq("id", 1);
		Bson greater_filter = gt("id", 3);
		Bson greater_or_equal_filter = gte("id", 4);
		Bson less = lt("id", 4);
		Bson less_or_equal_filter = lte("id", 5);

		/**** Connect to Mongo ****/
		// mongo_app.MongoConnect(host, port, dbname);

		/**** Create Collection ****/
		// mongo_app.create_collection(dbname, collectionName);

		/**** Get a list with all Collections ****/
		// p.collectionNames(dbname);

		/**** Drop Collection ****/
		// mongo_app.drop_collection(dbname, collectionName);

		/**** Insert a document in db - collection ****/
//		Document document = new Document("title", "The Ocean").append("id", 1).append("Artist", "Mike Perry")
//				.append("Album", "Berlin Tag & Nacht: Party & Feiern 2017").append("Year", 2016)
//				.append("Genre", "Tropical House");

		// mongo_app.insert_doc(dbname, collectionName, document);

		/**** Find and Print all documents from db ****/
		// mongo_app.get_docs(dbname, collectionName);

		/**** Read from filter ****/
		// mongo_app.find_doc(dbname, collectionName, equal_filter);

		/**** Delete from filter ****/
		// mongo_app.del_doc(dbname, collectionName,equal_filter );

		/**** Update a document ****/
		Bson update_filtering = set("id", 1);
		// mongo_app.update_doc(dbname,collectionName, equal_filter, update_filtering);

		/**** Close connection with Mongo ****/
		// mongo_app.close_connection();
	}

}
