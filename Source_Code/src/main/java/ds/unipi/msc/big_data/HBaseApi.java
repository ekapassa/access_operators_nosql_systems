package ds.unipi.msc.big_data;

/** HBase Client Imports **/
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Admin;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.Delete;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.filter.BinaryComparator;
import org.apache.hadoop.hbase.filter.CompareFilter.CompareOp;
import org.apache.hadoop.hbase.filter.Filter;
import org.apache.hadoop.hbase.filter.FilterList;
import org.apache.hadoop.hbase.filter.FilterList.Operator;
import org.apache.hadoop.hbase.filter.PrefixFilter;
import org.apache.hadoop.hbase.filter.QualifierFilter;
import org.apache.hadoop.hbase.util.Bytes;

/** Other Imports **/
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class HBaseApi {

	private static boolean isHBaseConnected;
	private static Connection conn;

	/*
	 * Connect to HBASE
	 */
	public boolean connectHBase(String host, int port, String path) {
		boolean result = false;

		Configuration config = HBaseConfiguration.create();

		config.set("hbase.zookeeper.quorum", host);
		config.set("hbase.zookeeper.property.clientPort", String.valueOf(port));
		config.set("zookeeper.znode.parent", path);

		try {
			conn = ConnectionFactory.createConnection(config);
			result = true;
			System.out.println("HBase Started!");
		} catch (IOException e) {
			e.printStackTrace();
		}

		setHBaseConnected(result);
		return result;
	}

	/*
	 * Close4 connection to HBASE
	 */
	public void closeHBase() {
		try {
			if (conn != null) {
				conn.close();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		setHBaseConnected(false);
	}

	public static boolean isHBaseConnected() {
		return isHBaseConnected;
	}

	public void setHBaseConnected(boolean isHBaseConnected) {
		this.isHBaseConnected = isHBaseConnected;
	}

	/*
	 * Create table with family
	 */
	public static void create_table(TableName tableName, String family) {

		HTableDescriptor tableDescriptor = new HTableDescriptor(tableName);
		tableDescriptor.addFamily(new HColumnDescriptor(family));

		try {
			Admin admin = conn.getAdmin();
			admin.createTable(tableDescriptor);
			boolean tableAvailable = admin.isTableAvailable(tableName);
			System.out
					.println("Is table = " + tableName + " with family = " + family + " available? " + tableAvailable);
		} catch (Exception e) {
			System.out.println("Table already exists: " + e.getMessage());
		}
	}

	/*
	 * Delete a table
	 */
	public static void del_table(TableName tableName) {

		Admin admin = null;
		try {
			admin = conn.getAdmin();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			System.out.println("Disabling table...");
			admin.disableTable(tableName);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			System.out.println("Deleting table...");
			admin.deleteTable(tableName);
			System.out.println("Table " + tableName + " deleted succesfully!");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/*
	 * Check if specific table exist
	 */
	public static boolean table_exist(TableName table_name) {

		boolean result = false;
		Admin admin = null;
		try {
			admin = conn.getAdmin();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		boolean bool;
		try {
			bool = admin.tableExists(table_name);
			result = true;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (result == true) {
			System.out.println("Table: " + table_name + " exists.");

		} else {
			System.out.println("Table: " + table_name + " does not exist.");

		}
		return result;
	}

	/*
	 * Disable a table
	 */
	public static boolean disable_table(TableName table_name) {

		boolean result = false;
		Admin admin = null;
		try {
			admin = conn.getAdmin();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// verifying whether the table is disabled or not
		Boolean isDisabled = null;
		try {
			isDisabled = admin.isTableDisabled(table_name);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		// disabling the table
		if (!isDisabled) {
			try {
				admin.disableTable(table_name);
				result = true;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		if (result == true) {
			System.out.println("Table: " + table_name + " disabled succesfully.");
		} else {
			System.out.println("Table: " + table_name + " already disabled.");

		}
		return result;
	}

	/*
	 * Enable a table
	 */
	public static boolean enable_table(TableName table_name) {

		boolean result = false;
		Admin admin = null;
		try {
			admin = conn.getAdmin();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// verifying whether the table is disabled or not
		Boolean isEnabled = null;
		try {
			isEnabled = admin.isTableEnabled(table_name);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		// disabling the table
		if (!isEnabled) {
			try {
				admin.enableTable(table_name);
				result = true;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (result == true) {
			System.out.println("Table: " + table_name + " enabled succesfully.");
		} else {
			System.out.println("Table: " + table_name + " already enabled.");

		}
		return result;
	}

	/*
	 * Add column to table
	 */
	public static boolean add_column_to_table(TableName table_name, String column) {

		boolean result = false;
		Admin admin = null;
		try {
			admin = conn.getAdmin();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		HColumnDescriptor columnDescriptor = new HColumnDescriptor(column);
		try {
			admin.addColumn(table_name, columnDescriptor);
			result = true;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (result == true) {
			System.out.println("Column family: " + column + " added to table: " + table_name);
		} else {
			System.out.println(
					"Column family '" + column + "' in table '" + table_name + "' already exists so cannot be added");
		}
		return result;

	}

	/*
	 * Delete column from table
	 */
	public static boolean delete_column(TableName table_name, String column) {

		boolean result = false;
		Admin admin = null;
		try {
			admin = conn.getAdmin();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			admin.deleteColumn(table_name, column.getBytes());
			result = true;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (result == true) {
			System.out.println("Column family: " + column + " deleted from " + table_name);
		} else {
			System.out.println("Column family '" + column + "' in table '" + table_name + "' cannot e deleted.");
		}
		return result;
	}

	/*
	 * Add data to column family
	 */
	public static void add_data(TableName tableName, String rowId, String family, String qualifier, String value) {

		Table table = null;
		try {
			table = conn.getTable(tableName);
		} catch (IOException e) {
			System.out.println("Cannot get table. Error: " + e.getMessage());
		}
		try {
			Admin admin = conn.getAdmin();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Put put = new Put(rowId.getBytes());
		put.addColumn(family.getBytes(), qualifier.getBytes(), value.getBytes());

		try {
			table.put(put);
			System.out.println("Data added succesfully column family: " + family + " in table: " + tableName);
		} catch (IOException e) {
			System.out.println("Cannot add data. Error: " + e.getMessage());

		}

	}

	/*
	 * Scan Table data
	 */
	public static void scan_table(TableName table_name, String family, String qualifier) {

		Scan s = new Scan();
		ResultScanner scanner = null;
		try {
			scanner = conn.getTable(table_name).getScanner(s);
		} catch (IOException e) {
			System.out.println("Cannot scan table. Error: " + e.getMessage());
		}
		try {
			for (Result result = scanner.next(); result != null; result = scanner.next()) {

				byte[] row = result.getValue(Bytes.toBytes(family), Bytes.toBytes(qualifier));
				String raw_result = Bytes.toString(row);

				System.out.println("Result: " + raw_result);
			}

		} catch (IOException e) {
			System.out.println("Cannot scan table. Error: " + e.getMessage());
		}
	}

	/*
	 * Update table data
	 */
	public static void update_table(TableName tableName, String rowId, String family, String qualifier, String value) {

		Put put = new Put(rowId.getBytes());
		put.addColumn(family.getBytes(), qualifier.getBytes(), value.getBytes());

		Delete delete = new Delete(rowId.getBytes());
		delete.addColumn(family.getBytes(), qualifier.getBytes());
		try {
			Table table = conn.getTable(tableName);
			table.put(put);
			System.out.println("Record succesfully updated");

		} catch (IOException e) {
			System.out.println("Update Record was unsucessfull" + e.getMessage());
		}
	}

	/*
	 * Scan table rows based on a filter
	 */
	public static void filter_table(TableName table_name, String family, String qualifier, List<Filter> filtering) {

		Scan s = new Scan();
		s.setFilter(new FilterList(Operator.MUST_PASS_ALL, filtering));

		ResultScanner scanner = null;
		try {
			scanner = conn.getTable(table_name).getScanner(s);
		} 
		catch (IOException e) {
			System.out.println("error: " + e.getMessage());

		}
		try {
			if (scanner != null) {
				int counter = 0;
				for (Result rr = scanner.next(); rr != null; rr = scanner.next()) {
					byte[] row = rr.getValue(Bytes.toBytes(family), Bytes.toBytes(qualifier));
					String raw_result = Bytes.toString(row);
					System.out.println("Filtered row: " + raw_result);
					counter ++;
				} 
				if (counter == 0) {
					System.out.println("No mutching rows for this filter ");
				}
			}
		} 
		catch (IOException e) {
			System.out.println("error: " +e.getMessage());
		}
	}

}
