package ds.unipi.msc.big_data;

import java.util.Iterator;
import org.bson.Document;
import org.bson.conversions.Bson;
import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.result.UpdateResult;

public class MongoApi {

	private static boolean isMongoConnected;
	private static MongoClient mongo_client;
	private MongoDatabase mongo_db;

	/*
	 * Make connection to MongoDB
	 */
	public boolean MongoConnect(String host_name, int host_port, String dbname) {
		boolean result = false;

		try {
			mongo_client = new MongoClient(host_name, host_port);
			mongo_db = mongo_client.getDatabase(dbname);
			result = true;

			setMongoConnected(result);
		} catch (Exception e) {
			System.out.println(e.getStackTrace());
		}
		System.out.println("Connected to MongoDB? " + result);
		return result;
	}

	/*
	 * Close connection to MongoDB
	 */
	public void close_connection() {
		if (mongo_client != null) {
			mongo_client.close();
		}
		setMongoConnected(false);
	}

	/*
	 * Is MongoDB successfully connected ?
	 */
	public static boolean isMongoConnected() {
		return isMongoConnected;
	}

	public void setMongoConnected(boolean isMongoConnected) {
		this.isMongoConnected = isMongoConnected;
	}

	/*
	 * Create a collection
	 */
	public boolean create_collection(String db_name, String collection_name) {
		boolean result = false;

		try {
			MongoDatabase database = mongo_client.getDatabase(db_name);
			MongoCollection<Document> coll = database.getCollection(collection_name);
			result = true;
		} catch (Exception e) {
			System.out.println(e.getStackTrace());
		}
		System.out.println("Collection " + collection_name + " created?  " + result);
		return result;
	}

	/*
	 * List all the available collections inside a Mongo database
	 */
	public void collectionNames(String db_name) {

		MongoDatabase database = mongo_client.getDatabase(db_name);

		for (String name : database.listCollectionNames()) {
			System.out.println(name);
		}
	}

	/*
	 * Insert a document into a collection
	 */
	public static void insert_doc(String db_name, String collection_name, Document Doc) {

		MongoDatabase database = mongo_client.getDatabase(db_name);
		MongoCollection<Document> collection = database.getCollection(collection_name);

		collection.insertOne(Doc);
		System.out.println("Document inserted successfully");
	}

	/*
	 * Delete a document from a collection
	 */
	public static void del_doc(String db_name, String collection_name, Bson filter) {

		MongoDatabase db = mongo_client.getDatabase(db_name);
		MongoCollection<Document> collection = db.getCollection(collection_name);

		try {
			collection.deleteMany(filter);
			// collection.deleteOne(filter);
			System.out.println("Document with filter: " + filter + ", deleted successfully");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * List all the available documents in a collection
	 */
	public static void get_docs(String db_name, String collection_name) {

		MongoDatabase database = mongo_client.getDatabase(db_name);
		MongoCollection<Document> collection = database.getCollection(collection_name);

		FindIterable<Document> iter = collection.find();
		int i = 1;
		// Getting the iterator
		Iterator<Document> it = iter.iterator();
		while (it.hasNext()) {
			System.out.println(it.next());
			i++;
		}
	}

	/*
	 * Find a document in a collection
	 */
	public static void find_doc(String db_name, String collection_name, Bson filter) {

		MongoDatabase database = mongo_client.getDatabase(db_name);
		MongoCollection<Document> collection = database.getCollection(collection_name);

		Document myDoc = collection.find(filter).first();
		System.out.println("Filtered Document: " + myDoc.toJson());
	}

	/*
	 * Update a document in a collection
	 */
	public static void update_doc(String db_name, String collection_name, Bson filter, Bson filter_update) {

		MongoDatabase database = mongo_client.getDatabase(db_name);
		MongoCollection<Document> collection = database.getCollection(collection_name);

		UpdateResult result = collection.updateOne(filter, filter_update);
		System.out.println("Document update successfully...");

		FindIterable<Document> iterDoc = collection.find();
		int i = 1;

		Iterator<Document> it = iterDoc.iterator();

		while (it.hasNext()) {
			System.out.println(it.next());
			i++;
		}
	}

	/*
	 * Delete (drop) a collection from a Mongo database
	 */
	public static void drop_collection(String db_name, String collection_name) {

		MongoDatabase database = mongo_client.getDatabase(db_name);
		MongoCollection<Document> collection = database.getCollection(collection_name);

		collection.drop();
		System.out.println("Collection " + collection_name + " dropped successfully");

	}

}
